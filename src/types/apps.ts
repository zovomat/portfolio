export type AppState = {
    apps: {
        [id: string]: App
    }
}

export type App = {
    id: string;
    name: string;
    description: string;
    route: string;
    icon: String;
    defaults: AppCoordinates;
    coords: AppCoordinates;
    status: AppMode;
};

export type AppCoordinates = {
    width: number;
    height: number;
    x: number;
    y: number;
}

export type AppMode = {
    isOpen: boolean;
    mode: 'DEFAULT' | 'MINIMIZED' | 'MAXIMIZED';
    top: boolean;
}