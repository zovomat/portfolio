import { App, AppMode, AppCoordinates } from "./apps"

export const ADD_APP = 'ADD_APP'
export const REMOVE_APP = 'REMOVE_APP'
export const OPEN_APP = 'OPEN_APP'
export const CLOSE_APP = 'CLOSE_APP'
export const CHANGE_APP_MODE = 'CHANGE_APP_MODE'
export const MOVE_APP = 'MOVE_APP'
export const PUT_APP_ON_TOP = 'PUT_APP_ON_TOP'

export interface AddAppAction {
    type: typeof ADD_APP;
    payload: App;
}
export interface RemoveAppAction {
    type: typeof REMOVE_APP;
    payload: {
        id: string;
    };
}
export interface OpenAppAction {
    type: typeof OPEN_APP;
    payload: {
        id: string;
    };
}
export interface CloseAppAction {
    type: typeof CLOSE_APP;
    payload: {
        id: string;
    };
}
export interface ChangeAppModeAction {
    type: typeof CHANGE_APP_MODE;
    payload: {
        id: string;
        status: AppMode;
    };
}
export interface MoveAppAction {
    type: typeof MOVE_APP;
    payload: {
        id: string;
        coords: AppCoordinates;
    };
}
export interface PutAppOnTopAction {
    type: typeof PUT_APP_ON_TOP;
    payload: {
        id: string;
    };
}

export type AppActionTypes =
 AddAppAction
 | RemoveAppAction
 | OpenAppAction
 | CloseAppAction
 | ChangeAppModeAction
 | MoveAppAction
 | AppCoordinates
 | PutAppOnTopAction;