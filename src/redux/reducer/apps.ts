import { AppActionTypes } from "../../types/appActions";

const initialState = {}

export default function todos(state = initialState, action: AppActionTypes) {
    switch (action.type) {
        case ADD_TODO:
        return [
            ...state,
            {
            id: state.reduce((maxId, todo) => Math.max(todo.id, maxId), -1) + 1,
            completed: false,
            text: action.text
            }
        ]
    }
}
export const ADD_TODO = 'ADD_TODO';
