import React from 'react';
import ReactDOM from 'react-dom';
import Shell from './Shell';
import * as serviceWorker from './serviceWorker';
import Providers from './context/Providers';

ReactDOM.render(
  <Providers>
    <Shell />
  </Providers>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
