import React, { useContext } from 'react';
import styled from 'styled-components';
import Header from './components/Header';
import Settings from './components/Settings';
import { AppContext } from './context/AppContext';
import { map } from 'lodash';
import App from './components/app/App';
const Main = styled.div`
  width: 100vw;
  height: 100vh;
  background-image: ${`url(https://source.unsplash.com/collection/176316/${
    Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0)
  }x${
    Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)
  })`};
  background-repeat: no-repeat;
  background-size: cover;
  overflow: hidden;
`;

function Shell() {
  const appContext = useContext(AppContext);
  return (
    <Main className="App">
      <Header />
      <Settings />
      {map(
        appContext.apps,
        (app, key) => (
          <App id={app.id} key={key} />
        )
      )}
    </Main>
  );
}

export default Shell;
