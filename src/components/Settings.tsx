import React from 'react';
import styled from 'styled-components';
import { Settings2Outline } from '@styled-icons/evaicons-outline/Settings2Outline';

const Container = styled.nav`
    position: absolute;
    top: 0;
    right: 0;
    height: 48px;
    width: max-content;
    background: ${({ theme }) => theme.colors.background};
`;
const Icon = styled(Settings2Outline)`
    color: ${({ theme }) => theme.colors.text };
    width: 32px;
    height: 32px;
    margin: 8px;
`;

function Header() {
    return (
        <Container>
            <Icon
                height={32}
                width={32}
            />
        </Container>
    );
  }
  
  export default Header;
  