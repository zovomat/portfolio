import React, { FunctionComponent } from "react"
import { AppData } from "../../context/AppContext";
import { Rnd } from 'react-rnd';
import styled from "styled-components";


type AppProps = {
    data: AppData;
};

const OuterFrame = styled(Rnd)`
    background: ${({ theme }: any) => theme.colors.background };
`;

const InnerFrame = styled.div`
    background: ${({ theme }: any) => theme.colors.text };
    width: 100%;
    margin-top: 32px;
    height: calc(100% - 32px);
    
`;

const App: FunctionComponent<AppProps> = ({ data }) => {
    return (
        <OuterFrame
            cancel=".cancel"
            default={{
                width: data.state.width,
                height: data.state.height,
                x: data.state.x,
                y: data.state.y,
            }}
        >
            <InnerFrame className="cancel" />   
        </OuterFrame>
    )
};

export default App;