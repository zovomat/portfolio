import React from 'react';
import styled from 'styled-components';
import { ArrowIosDownwardOutline } from '@styled-icons/evaicons-outline/ArrowIosDownwardOutline';

const Container = styled.nav`
    position: absolute;
    top: 0;
    left: 0;
    height: 48px;
    width: max-content;
    background: ${({ theme }) => theme.colors.background};
    display: flex;
    flex-direction: row;
`;

const Icon = styled(ArrowIosDownwardOutline)`
    color: ${({ theme }) => theme.colors.text };
    width: 32px;
    height: 32px;
    margin: 8px;
`;

const Text = styled.p`
    color: ${({ theme }) => theme.colors.text};
    margin: 8px;
    margin-right: 16px;
    line-height: 32px;
    font-size: 24px;
`;

function Header() {
    return (
        <Container>
            <Icon
                height={32}
                width={32}
            />
            <Text>
                Hello Motherfucker
            </Text>
        </Container>
    );
  }
  
  export default Header;
  