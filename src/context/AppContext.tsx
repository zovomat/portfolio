import React, { createContext, FunctionComponent, useReducer, useContext, useCallback } from "react";
import { Context } from "vm";

type ShellData = {
    apps: {
        [id: string]: AppData;
    };
};

export type AppData = {
    id: string;
    order: number;
    route: string;
    label: string;
    state: {
        width: string;
        height: string;
        x: number;
        y: number;
        condition: 'window' | 'maximized' | 'minimized' | 'closed';
        stack: number;
    }
    constraints: {
        minWidth: string;
        minHeight: string;
        canBeMaximized: boolean;
        canBeMinimized: boolean;
    }
};

const mockData: ShellData = {
    apps: {
        'zvm-hello': {
            id: 'zvm-hello',
            order: 0,
            route: 'nanana',
            label: 'Hello',
            state: {
                width: '30vw',
                height: '50vh',
                x: 100,
                y: 100,
                condition: 'window',
                stack: 0
            },
            constraints: {
                minWidth: '256px',
                minHeight: '256px',
                canBeMaximized: true,
                canBeMinimized: true
            }
        }
    }
};

type AppDataAction = {
    type: string;
    id: string;
    payload: any;
}

const reducer = (state: ShellData, action: AppDataAction): ShellData => {
    console.log(action);
    return state;
}

type ShellContext = {
    apps: { [id: string]: AppData };
    dispatch: React.Dispatch<AppDataAction> | undefined;
}

export const AppContext = createContext<ShellContext>({ apps: mockData.apps, dispatch: undefined });

export const AppContextProvider: FunctionComponent = ({ children }) => {
    const [data, dispatch] = useReducer<(state: ShellData, action: AppDataAction) => ShellData>(reducer, mockData);
    return (
        <AppContext.Provider
            value={{
                apps: data.apps,
                dispatch
            }}
        >
            {children}
        </AppContext.Provider>
    );
};
