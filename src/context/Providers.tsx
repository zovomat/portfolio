import React, { FunctionComponent } from "react";
import { AppContextProvider } from "./AppContext";
import ThemeProvider from "./ThemeContext";
import { createStore } from 'redux';
import reducer from '../redux/reducer';
import { Provider } from 'react-redux'

const store = createStore(reducer);

const Providers: FunctionComponent = ({ children }) => {
 return (
    <ThemeProvider>
        <AppContextProvider>
            <Provider store={store}>
                {children}
            </Provider>
        </AppContextProvider>
    </ThemeProvider>
 );
};

export default Providers;