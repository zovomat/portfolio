import { createGlobalStyle, ThemeProvider as StyledThemeProvider } from "styled-components";
import { FunctionComponent } from "react";
import React from "react";

const Global = createGlobalStyle`
body {
    overflow: hidden;
}
* {
  margin: 0;
  font-family: 'Raleway', sans-serif;
}
`;

const customTheme = {
    colors: {
        background: 'hsl(220, 15%, 5%)',
        text: 'hsl(220, 5%, 85%)'
    }
}

const ThemeProvider: FunctionComponent = ({ children }) => (
    <StyledThemeProvider theme={customTheme}>
        <Global />
        {children}
    </StyledThemeProvider>
);

export default ThemeProvider